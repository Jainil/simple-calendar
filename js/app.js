var app = app || {};

(function () {
  "use strict";

  app.control = {
    currentDate: new Date(),

    goNextDay: function () {
      this.currentDate.setDate(this.currentDate.getDate() + 1);
      this.updateDateView();
    },

    goPrevDay: function () {
      this.currentDate.setDate(this.currentDate.getDate() - 1);
      this.updateDateView();
    },

    goToday: function () {
      this.currentDate = new Date();
      this.updateDateView();
    },

    updateDateView: function () {
      var events = app.model.getEventsForDate(this.currentDate);

      app.view.renderDate(this.currentDate, events);
    },

    init: function () {
      app.model.init(function () {
        app.view.init();
        this.updateDateView();
      }.bind(this));
    }
  };

  app.control.init();
})();
