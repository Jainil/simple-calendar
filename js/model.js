var app = app || {};

(function () {
  "use strict";

  app.model = {
    init: function (cb) {
      this.data = _data.map(function (event) {
        return {
          startTime: new Date(event.startTime),
          endTime: new Date(event.endTime),
          title: event.title
        }
      });
      cb();
    },

    getEventsForDate: function (date) {
      var events = this.data
        .filter(function (event) {
          return (app.util.isSameDate(date, event.startTime) || app.util.isSameDate(date, event.endTime));
        })
        .map(function (event) {
          return {
            startTime: new Date(event.startTime),
            endTime: new Date(event.endTime),
            title: event.title,
            concurrent: {
              count: 0
            }
          }
        });

      events.forEach(function (event) {
        events.forEach(function (otherEvent) {
          if (!(event == otherEvent)) {
            if (!(event.startTime >= otherEvent.endTime || otherEvent.startTime >= event.endTime)) {
              event.concurrent.count += 1;
              if (otherEvent.concurrent.offset !== undefined) {
                event.concurrent.offset = otherEvent.concurrent.offset + 1;
              } else if (event.concurrent.offset === undefined) {
                event.concurrent.offset = 0;
              }
            }
          }
        })
      });

      return events;
    }
  };

})();
