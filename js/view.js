var app = app || {};

(function() {
  "use strict";

  var util = app.util;

  var CONST = {
    MIN_INTERVAL: 15, // in minutes
    MIN_HEIGHT: 20 // in px
  };


  app.view = {
    init: function () {
      var timeList = document.querySelector('.time-list'),
        nextButton = document.querySelector('#btn-next'),
        prevButton = document.querySelector('#btn-prev'),
        todayButton = document.querySelector('#btn-today');

      nextButton.addEventListener('click', function (e) {
        app.control.goNextDay();
      });

      prevButton.addEventListener('click', function (e) {
        app.control.goPrevDay();
      });

      todayButton.addEventListener('click', function (e) {
        app.control.goToday();
      });

      for (var i = 0; i <= 24; i++) {
        timeList.appendChild(createTimeDisp(i));
      }

      function createTimeDisp(hour) {
        var time = document.createElement('span');

        time.textContent = util.formatAmpm(hour, 0);
        time.style.position = 'absolute';
        time.style.top = (((hour * 60) / CONST.MIN_INTERVAL) * CONST.MIN_HEIGHT) + 'px';
        return time;
      }
    },

    renderDate: function (date, events) {
      var eventList = document.querySelector('.event-list'),
        todayDisp = document.querySelector('.today-disp'),
        eventTpl = document.querySelector('#event-template').textContent,
        eventPopupTpl = document.querySelector('#event-popup-template').textContent;

      todayDisp.textContent = date.toDateString();

      util.clearNode(eventList);

      events.forEach(function (event) {
        createEventDisp(eventList, event);
      });

      function createEventDisp(container, event) {
        var elem = document.createElement('div');
        elem.className = 'event-item';

        if (event.startTime.getDate() !== date.getDate()) {
          event.startTime.setDate(date.getDate());
          event.startTime.setHours(0);
          event.startTime.setMinutes(0);
          event.title += ' (continued)';
        }

        if (event.endTime.getDate() !== date.getDate()) {
          event.endTime.setDate(date.getDate());
          event.endTime.setHours(23);
          event.endTime.setMinutes(59);
          event.title += ' (continues)';
        }

        var eventData = {
          title: event.title,
          startTime: util.timeString(event.startTime),
          endTime: util.timeString(event.endTime)
        };

        elem.innerHTML = util.t(eventTpl, eventData);
        setEventPosAndDim(elem, event.startTime, event.endTime, event.concurrent);

        elem.addEventListener('click', createEventPopup(container, elem, eventData));

        container.appendChild(elem);
      }

      function setEventPosAndDim(elem, start, end, concurrent) {
        var sMin = start.getHours() * 60 + start.getMinutes(),
          eMin = end.getHours() * 60 + end.getMinutes();

        elem.style.height = (((eMin - sMin) / CONST.MIN_INTERVAL) * CONST.MIN_HEIGHT) + 'px';
        elem.style.top = ((sMin / CONST.MIN_INTERVAL) * CONST.MIN_HEIGHT) + 'px';
        elem.style.width = (100 / (concurrent.count + 1)) + '%';
        elem.style.left = ((concurrent.offset / (concurrent.count + 1)) * 100) + '%';
      }

      function createEventPopup(container, elem, eventData) {
        return function popupHandler(e) {
          elem.removeEventListener('click', popupHandler);

          var popup = document.createElement('div');
          popup.className = 'event-popup';
          popup.innerHTML = util.t(eventPopupTpl, eventData);
          popup.style.top = parseInt(elem.style.top) - 110 + 'px';

          var elemStyle = window.getComputedStyle(elem);
          var left = parseInt(elemStyle.left) ? parseInt(elemStyle.left) : 0;
          var arrowOffset = left + (parseInt(elemStyle.width) / 2) - 20;

          var popupElem = container.appendChild(popup);

          popupElem.querySelector('.btn-close').addEventListener('click', function (event) {
            eventList.removeChild(popup);
            elem.addEventListener('click', createEventPopup(container, elem, eventData));
          });

          popupElem.querySelector('.event-popup-arrow').style.left = arrowOffset + 'px';
        }

      }
    }
  };

})();
