var app = app || {};

(function() {
  "use strict";

  app.util = {
    clearNode: function (node) {
      while (node.firstChild) {
        node.removeChild(node.firstChild);
      }
    },

    // tweet-sized template engine
    t: function (s, d) {
      for (var p in d)
        s = s.replace(new RegExp('{' + p + '}', 'g'), d[p]);
      return s;
    },

    isSameDate: function (dateObj1, dateObj2) {
      return (dateObj1.getDate() == dateObj2.getDate()
      && dateObj1.getMonth() == dateObj2.getMonth()
      && dateObj1.getFullYear() == dateObj2.getFullYear())
    },

    timeString: function (dateObj) {
      var hours = dateObj.getHours(),
        minutes = dateObj.getMinutes();

      return this.formatAmpm(hours, minutes);
    },

    formatAmpm: function (hours, minutes) {
      var append, hh, mm;

      append = 24 > hours && hours >= 12 ? 'pm' : 'am';
      hh = hours % 12;
      hh = hh === 0 ? 12 : hh;
      hh = hh > 9 ? hh : '0' + hh;
      mm = minutes > 9 ? minutes : '0' + minutes;

      return hh + ':' + mm + (append ? ' ' + append.toUpperCase() : '');
    }
  };

})();
